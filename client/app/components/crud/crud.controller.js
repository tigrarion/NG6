class CrudController {
  constructor($http) {
      "ngInject";
    this.name = 'crud';
    this.products = [];
    this.$http = $http;
    var store = this;
    store.data = [
    {
        "id" : 1,
        "name" : "Den",
        "nick": "Roberts",
        "age" : 21
    },
    {
        "id": 2,
        "name": "Bobby",
        "nick": "Sanders",
        "age": 35
    },
    {
        "id": 3,
        "name": "Robertro",
        "nick": "Der Toro",
        "age": 27
    }
    ];
    store.data.push({"id": 4, "name": "Antonio"});
    
    this.addReview = function() {
        store.data.push(this.review);
        this.review = {};
    };
  }
  
}

export default CrudController;
